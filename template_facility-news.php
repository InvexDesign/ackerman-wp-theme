<?php
/*
Template Name: Facility News
*/
?>
<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<section id="main">
			<?php
	  		$banner_title = getAdvancedCustomFieldValue('banner-title', false);
			  $banner_image = getAdvancedCustomFieldValue('banner-image', false);
	  		if(!$banner_image && $post->post_parent)
				{
					$banner_image = getAdvancedCustomFieldValue('banner-image', false, $post->post_parent);
				}

				if(!$banner_title && $post->post_parent)
				{
					$banner_title = getAdvancedCustomFieldValue('banner-title', false, $post->post_parent);
				}

	  		$banner_image_url = $banner_image ? $banner_image['url'] : get_template_directory_uri() . '/assets/build/images/default-banner.png';
				include(get_template_directory() . '/_templates/_partials/short-banner.php');
			?>
			<div class="content">
				<?php
					$sidebar_menu_id = getAdvancedCustomFieldValue('sidebar-menu', false);
					if(!$sidebar_menu_id && $post->post_parent)
					{
						$sidebar_menu_id = getAdvancedCustomFieldValue('sidebar-menu', false, $post->post_parent);
					}

					$widgets = [];
					if($post->post_parent)
					{
						if(have_rows('sidebar-widget', $post->post_parent))
						{
							while(have_rows('sidebar-widget', $post->post_parent))
							{
								the_row();

								$widget = [
									'title' => get_sub_field('title'),
									'color' => get_sub_field('color'),
									'content' => get_sub_field('content')
							  ];
								$widgets[] = $widget;
							}
						}
					}

					if(have_rows('sidebar-widget'))
					{
						while(have_rows('sidebar-widget'))
						{
							the_row();

							$widget = [
								'title' => get_sub_field('title'),
								'color' => get_sub_field('color'),
								'content' => get_sub_field('content')
							];
							$widgets[] = $widget;
						}
					}
					include(get_template_directory() . '/_templates/_partials/sidebar.php');
				?>
				<div class="main-col">
					<?php $page_title = getAdvancedCustomFieldValue('custom-page-title', get_the_title()); ?>
					<h3><?php echo $page_title; ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
					<div class="content-wrap">
						<?php
							$page_title = get_the_title();
							$banner_title = 'News';
							$banner_image_url = get_template_directory_uri() . '/assets/images/banners/news.png';
							$sidebar_menu_id = 71;
							$sidebar_widget_area_id = 'news_sidebar_area';

							$show_post_date = true;
							$show_post_date = true;
							$posts_per_page = 20;
							$paged = get_query_var('paged') ? get_query_var('paged') : 1;
							$wp_query_parameters = [
									'posts_per_page' => $posts_per_page,
									'paged'          => $paged,
									'order'          => 'desc',
							];
							if(defined('FACILITY_NEWS_POST_CATEGORY_ID') && is_int(FACILITY_NEWS_POST_CATEGORY_ID))
							{
								$wp_query_parameters['cat'] = FACILITY_NEWS_POST_CATEGORY_ID;
							}
						?>
	<?php endwhile; ?>
						<?php if(!isset($wp_query_parameters)) : ?>
							<p>Please set post query parameters!</p>
						<?php else : ?>
							<?php $wp_query = new WP_Query($wp_query_parameters);?>
							<?php if($wp_query->have_posts()) : ?>
								<table class="alt pic">
									<tbody>
									<?php while($wp_query->have_posts()) : $wp_query->the_post(); ?>
										<tr>
											<td>
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												<?php if(isset($show_post_date) && $show_post_date) : ?><p class="post-date"><?php the_date(); ?></p><?php endif; ?>
												<p><?php
														//This removes any images from the content
														$content = get_the_content('Read More &raquo;');
														$content = preg_replace("/<img[^>]+\>/i", " ", $content);
														$content = apply_filters('the_content', $content);
														$content = str_replace(']]>', ']]>', $content);
														echo $content;
														?></p>
											</td>
											<td><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail', array('class' => 'alignleft')); ?></a></td>
										</tr>
									<?php endwhile; ?>
									</tbody>
								</table>
								<div class="nav-previous alignleft"><?php next_posts_link('&laquo; Older Posts'); ?></div>
								<div class="nav-next alignright"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
								<div class="clearer"></div>
								<?php wp_reset_postdata(); ?>
							<?php else : ?>
								<p>Sorry, there are no posts at this time.</p>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
<?php endif; ?>

<?php get_footer(); ?>