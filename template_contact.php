<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<section id="main" class="main full-width">
			<?php
			$banner_title = getAdvancedCustomFieldValue('banner-title', false);
			$banner_image = getAdvancedCustomFieldValue('banner-image', false);
			if(!$banner_image && $post->post_parent)
			{
				$banner_image = getAdvancedCustomFieldValue('banner-image', false, $post->post_parent);
			}

			if(!$banner_title && $post->post_parent)
			{
				$banner_title = getAdvancedCustomFieldValue('banner-title', false, $post->post_parent);
			}

			if($banner_title && $banner_image)
			{
				$banner_image_url = $banner_image['url'];
				include(get_template_directory() . '/_templates/_partials/short-banner.php');
			}
			?>
			<div class="content">
				<div class="main-col">
					<?php $page_title = getAdvancedCustomFieldValue('custom-page-title', get_the_title()); ?>
					<h3><?php echo $page_title; ?><?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
                    <div class="content-wrap" style="display: flex; flex-direction: column;">
                        <div style="display: flex; flex-direction: row; flex-wrap: wrap; justify-content: space-between;">
                            <div style="flex-basis: 600px;">
                                <?php the_content(); ?>
                            </div>
                            <div style="flex: 1; text-align: center;">
                                <a href="<?php echo home_url('/'); ?>about/staff" class="callout" style="margin-top: 35px;">Browse the <span><i class="fa fa-users" aria-hidden="true"></i> Staff Directory</span></a>
                            </div>
                        </div>
                        <hr />
                        <div style="display: flex; flex-direction: row; flex-wrap: wrap; justify-content: space-around;">
                            <div style="flex-basis: 450px;">
															<?php echo do_shortcode('[wufoo username="glenellynpd" formhash="z1xsbaj20s5e7sq" autoresize="true" height="600" header="show" ssl="true"]'); ?>
                            </div>
                            <div style="flex-basis: 450px;">
                                <iframe style="max-width: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23759.916941240936!2d-88.07383592618142!3d41.89308037235316!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e52c460d06ba7%3A0xb04885dfddcc8e37!2sAckerman+Sports+%26+Fitness+Center!5e0!3m2!1sen!2sus!4v1514586873256" style="border:0" allowfullscreen="" width="500" height="450" frameborder="0"></iframe>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>