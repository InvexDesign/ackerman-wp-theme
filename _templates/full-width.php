<section id="main">
	<div class="content">
		<div class="main-col full-width">
			<h3><?php echo get_the_title(); ?> <?php include('_partials/sharethis.php'); ?></h3>
			<div class="content-wrap">
				<?php if(isset($content)) : ?>
					<?php echo $content; ?>
				<?php else : ?>
					<?php the_content(); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>