<?php
//TODO: How to handle target blank links?
//TODO: Extract this to header
$current_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<div class="sidebar">
	<?php if(isset($sidebar_menu_id) && $sidebar_menu_id) : ?>
	<div class="widget">
		<div class="menu">
			<ul>
				<?php $menu_items = wp_get_nav_menu_items($sidebar_menu_id); ?>
				<?php foreach($menu_items as $menu_item) : ?>
					<li><a href="<?php echo $menu_item->url; ?>" <?php echo ($current_url == $menu_item->url) ? 'class="current"' : ''; ?> ><?php echo $menu_item->title; ?></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
	</div>
	<?php endif; ?>
	<?php foreach($widgets as $widget) : ?>
		<div class="widget non-menu <?php echo $widget['color']; ?>">
			<h4><?php echo $widget['title']; ?></h4>
			<div><?php echo $widget['content']; ?></div>
		</div>
	<?php endforeach;?>
</div>