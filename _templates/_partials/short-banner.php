<div class="banner short-banner" style="background-image: url(<?php echo isset($banner_image_url) ? $banner_image_url : 'please-set-image-url'; ?>)">
	<div class="banner-inner">
		<?php if(isset($banner_title)) : ?>
			<h2><span><?php echo $banner_title; ?></span></h2>
	  <?php endif; ?>
	</div>
</div>