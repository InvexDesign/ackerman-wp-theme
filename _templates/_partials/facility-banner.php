<?php /* TODO: For some reason this only works with the url from the GMaps EMBED code */ ?>
<div class="banner facility wide aqua" style="height: 325px;">
	<div class="banner-inner">
		<div class="wide-container">
			<?php /*$metaslider_id = get_post_meta( get_the_ID(), 'metaslider_id', true);*/ ?>
			<?php if(isset($metaslider_id) && filter_var($metaslider_id, FILTER_VALIDATE_INT)) : ?>
				<?php echo do_shortcode("[metaslider id=$metaslider_id]"); ?>
			<?php else : ?>
				<?php echo "Invalid metaslider_id provided"; ?>
			<?php endif; ?>
		</div>
		<h2><span><?php echo isset($banner_title) ? $banner_title : 'Please set title!'; ?></span></h2>
		<?php if(isset($google_maps_url)) : ?>
			<a href="<?php echo $google_maps_url; ?>&output=embed" class="gmap" data-lity><i class="icon fa fa-lg fa-map" aria-hidden="true"></i> &nbsp;&nbsp;Google Map</a>
		<?php endif; ?>
	</div>
</div>