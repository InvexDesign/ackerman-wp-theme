<?php
require_once('includes/constants.php');
require_once('includes/helpers.php');
require_once('includes/admin-scripts.php');
require_once('includes/register-nav-menus.php');
require_once('includes/advanced-custom-fields.php');
require_once('custom-post-types/accordion.php');
require_once('shortcodes/accordion.php');

add_theme_support('menus');
add_theme_support('post-thumbnails');

$tile_size = 200;
add_image_size('masonry-1x1', $tile_size, $tile_size);
add_image_size('masonry-1x2', 2 * $tile_size, $tile_size);
add_image_size('masonry-2x1', $tile_size, 2 * $tile_size);
add_image_size('masonry-2x2', 2 * $tile_size, 2 * $tile_size);

function enqueueAssets()
{
	$css = [
		'app'           => '/assets/build/css/app.min.css',
		'search-main'   => '/search/search.min.css',
		'search-custom' => '/search/search-custom.css',
	];

	foreach($css as $handle => $filepath)
	{
		wp_enqueue_style($handle, get_template_directory_uri() . $filepath, [], filemtime(get_template_directory() . $filepath), false);
	}


	wp_register_script('app-js', get_template_directory_uri() . '/assets/build/js/bundle.min.js');
	wp_enqueue_script('app-js');
}

add_action('wp_enqueue_scripts', 'enqueueAssets');

global $sidebar_widget_areas;
$sidebar_widget_areas = [
	'sidebar_1' => 'Sidebar #1',
	'sidebar_2' => 'Sidebar #2',
];
function register_widgetized_areas()
{
	global $sidebar_widget_areas;
	foreach($sidebar_widget_areas as $id => $name)
	{
		register_sidebar([
			'name'          => $name,
			'id'            => $id,
			'before_widget' => '<div class="widget non-menu">',
			'after_widget'  => '</div>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>',
		]);
	}
}
add_action('widgets_init', 'register_widgetized_areas');

//TODO: conditionally perform this?
function acf_load_sidebar_widget_area_field_choices($field)
{
	$field['choices'] = array();

	global $sidebar_widget_areas;
	foreach($sidebar_widget_areas as $id => $menu)
	{
		$field['choices'][$id] = $menu;
	}

	return $field;
}
add_filter('acf/load_field/name=sidebar-widget-area', 'acf_load_sidebar_widget_area_field_choices');

function acf_load_sidebar_menu_field_choices($field)
{
	$field['choices'] = [
		false => '* Inherit Parent Sidebar Menu *'
	];

	foreach(get_all_wordpress_menus() as $menu)
	{
		$field['choices'][$menu->term_id] = $menu->name;
	}

	return $field;
}
add_filter('acf/load_field/name=sidebar-menu', 'acf_load_sidebar_menu_field_choices');

// Custom TinyMCE Editor Styles
function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

function my_mce_before_init_insert_formats( $init_array ) {

// Define the style_formats array

	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title' => 'PDF Link',
			'inline' => 'a',
			'selector' => 'a',
			'classes' => 'pdf',
			'wrapper' => false,

		),
		array(
			'title' => 'PDF List',
			'block' => 'ul',
			'selector' => 'ul',
			'classes' => 'pdf',
			'wrapper' => true,
		),
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


function mprd_add_editor_styles() {
	add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'mprd_add_editor_styles' );

function wpb_disable_feed() {
	wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}
add_action('do_feed', 'wpb_disable_feed', 1);
add_action('do_feed_rdf', 'wpb_disable_feed', 1);
add_action('do_feed_rss', 'wpb_disable_feed', 1);
add_action('do_feed_rss2', 'wpb_disable_feed', 1);
add_action('do_feed_atom', 'wpb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'wpb_disable_feed', 1);
add_action('do_feed_atom_comments', 'wpb_disable_feed', 1);

function override_mce_options($initArray) {
	$opts = '*[*]';
	$initArray['valid_elements'] = $opts;
	$initArray['extended_valid_elements'] = $opts;
	return $initArray;
}
add_filter('tiny_mce_before_init', 'override_mce_options');

if(defined('INVEX_SLIDERS_URL') && INVEX_SLIDERS_URL)
{
	function addInvexSlidersButton($wp_admin_bar) {
		$args = [
			'id'    => 'invex_sliders_button',
			'title' => '<span class="dashicons dashicons-images-alt2 ab-icon" style="margin-top: 3px;"></span><span class="ab-label">Invex Sliders</span>',
			'href'  => INVEX_SLIDERS_URL,
		];
		$wp_admin_bar->add_node($args);
	}
	add_action('admin_bar_menu', 'addInvexSlidersButton', 50);
}

include('search/relevanssi-attachments.php');
include('search/relevanssi-exclusions.php');

require_once('invex-popups/invex-popups.php');