<?php

function register_theme_nav_menus()
{
	register_nav_menu('header-nav', __('Header Nav', 'theme-slug'));
	register_nav_menu('footer-col-one', __('Footer Column 1', 'theme-slug'));
	register_nav_menu('footer-col-two', __('Footer Column 2', 'theme-slug'));
}
//add_action('after_setup_theme', 'register_theme_nav_menus');