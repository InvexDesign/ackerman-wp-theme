<?php

function dd($stuff)
{
	var_dump($stuff);
	die;
}

function get_all_wordpress_menus()
{
	return get_terms('nav_menu', ['hide_empty' => true]);
}

function isUnderTab($tab, $url)
{
	$base_url = str_replace('/', '\\/', home_url());
	$regex = "/^$base_url\\/$tab.*$/";
//	echo "<span class=\"beansss\" style='display: none;'>";
//	var_dump([
//		$base_url,
//		$regex,
//		$tab,
//		$url
//	]);
//	echo "</span>";
	return preg_match($regex, $url);
}

//function new_excerpt_more($more) {
//	global $post;
////	remove_filter('excerpt_more', 'new_excerpt_more');
//	return " <a class=\"read-more\" href=\"". get_permalink($post->ID) . "\">Read More &raquo;</a>";
//}
//add_filter('excerpt_more','new_excerpt_more');

function getInvexSlides($metaslider_id)
{
	$args = [
		'force_no_custom_order' => true,
		'orderby'               => 'menu_order',
		'order'                 => 'ASC',
		'post_type'             => ['attachment', 'ml-slide'],
		'post_status'           => ['inherit', 'publish'],
		'lang'                  => '', // polylang, ingore language filter
		'suppress_filters'      => 1, // wpml, ignore language filter
		'posts_per_page'        => -1,
		'tax_query'             => [
			[
				'taxonomy' => 'ml-slider',
				'field'    => 'slug',
				'terms'    => $metaslider_id
			]
		]
	];

	$slides = [];
	$args = apply_filters( 'metaslider_populate_slides_args', $args, $metaslider_id, []);
	$query = new WP_Query( $args );
	while($query->have_posts())
	{
		$query->next_post();
		$post = $query->post;
		$featured_image = get_the_post_thumbnail_url($post->ID);
		$slides[] = [
			'image_url' => $featured_image ?: $post->guid,
			'title'     => get_post_meta($post->ID, 'ml-slider_title', true),
			'content'   => $post->post_excerpt,
			'link'      => get_post_meta($post->ID, 'ml-slider_url', true),
			'button'    => get_post_meta($post->ID, '_wp_attachment_image_alt', true)
		];
	}
	wp_reset_postdata();

	return $slides;
}

function get_browser_name($user_agent)
{
	if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
	elseif (strpos($user_agent, 'Edge')) return 'Edge';
	elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
	elseif (strpos($user_agent, 'Safari')) return 'Safari';
	elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
	elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';

	return 'Other';
}

function getMetaSlides($metaslider_id)
{
	$args = [
		'force_no_custom_order' => true,
		'orderby'               => 'menu_order',
		'order'                 => 'ASC',
		'post_type'             => ['attachment', 'ml-slide'],
		'post_status'           => ['inherit', 'publish'],
		'lang'                  => '', // polylang, ingore language filter
		'suppress_filters'      => 1, // wpml, ignore language filter
		'posts_per_page'        => -1,
		'tax_query'             => [
			[
				'taxonomy' => 'ml-slider',
				'field'    => 'slug',
				'terms'    => $metaslider_id
			]
		]
	];

	$slides = [];
	$args = apply_filters('metaslider_populate_slides_args', $args, $metaslider_id, []);
	$query = new WP_Query($args);
	while($query->have_posts())
	{
		$query->next_post();
		$post = $query->post;
		$image_url = get_the_post_thumbnail_url($post->ID);
		$slides[] = [
			'post_id'   => $post->ID,
			'image_url' => $image_url ?: $post->guid,
			'title'     => get_post_meta($post->ID, 'ml-slider_title', true),
			'content'   => $post->post_excerpt,
			'link'      => get_post_meta($post->ID, 'ml-slider_url', true),
			'button'    => get_post_meta($post->ID, '_wp_attachment_image_alt', true)
		];
	}
	wp_reset_postdata();

	return $slides;
}

function getMetaSliders()
{
	$sort_key = 'post_title';
	$sliders = [];

	// list the tabs
	$args = [
		'post_type'        => 'ml-slider',
		'post_status'      => 'publish',
		'orderby'          => $sort_key,
		'suppress_filters' => 1, // wpml, ignore language filter
		'order'            => 'ASC',
		'posts_per_page'   => -1
	];

	$args = apply_filters('metaslider_all_meta_sliders_args', $args);

	// WP_Query causes issues with other plugins using admin_footer to insert scripts
	// use get_posts instead
	$all_sliders = get_posts($args);
	foreach($all_sliders as $slideshow)
	{
		$sliders[] = [
			'title'  => $slideshow->post_title,
			'id'     => $slideshow->ID
		];
	}

	return $sliders;
}

function preProcessMenuItems($menu_id)
{
	$menu = [];
	$menu_items = wp_get_nav_menu_items($menu_id);

	foreach($menu_items as $menu_item)
	{
		$item = [
			'post_id'    => get_post_meta($menu_item->ID, '_menu_item_object_id', true),
			'name'       => $menu_item->title,
			'url'        => $menu_item->url,
		];

		if($parent_id = $menu_item->menu_item_parent)
		{
			if(isset($menu[$parent_id]))
			{
				if(!isset($menu[$parent_id]['children']))
				{
					$menu[$parent_id]['children'] = [$menu_item->ID => $item];
				}
				else
				{
					$menu[$parent_id]['children'][$menu_item->ID] = $item;
				}
			}
			else
			{
				//more than one level of sub menus
			}
		}
		else
		{
			$menu[$menu_item->ID] = $item;
		}
	}

	return $menu;
}

function getSidebarMenuItems($menu_id)
{
	$menu = [];
	$menu_items = wp_get_nav_menu_items($menu_id);

	foreach($menu_items as $menu_item)
	{
		$post_id = get_post_meta($menu_item->ID, '_menu_item_object_id', true);
		$item = [
			'post_id'    => $post_id,
			'name'       => $menu_item->title,
			'url'        => $menu_item->url,
		];

		if($parent_id = $menu_item->menu_item_parent)
		{
			$parent_id = get_post_meta($parent_id, '_menu_item_object_id', true);
			if(isset($menu[$parent_id]))
			{
				if(!isset($menu[$parent_id]['children']))
				{
					$menu[$parent_id]['children'] = [$post_id => $item];
				}
				else
				{
					$menu[$parent_id]['children'][$post_id] = $item;
				}
			}
			else
			{
				//more than one level of sub menus
			}
		}
		else
		{
			$menu[$post_id] = $item;
		}
	}

	return $menu;
}

function displayMultilevelResponsiveMenu($menu_id)
{
	$menu = preProcessMenuItems($menu_id);
	include(get_template_directory() . '/includes/responsive-menu.php');
}

function getAdvancedCustomFieldValue($field, $default = null, $post_id = false)
{
	$field = get_field_object($field, $post_id);
	if($field && $field['value'])
	{
		return $field['value'];
	}

	return $default;
}

function displayPhone($string, $default = '')
{
	if($phone = preg_replace('/[^0-9]/', '', $string))
	{
		$string = '(' . substr($phone, 0, 3) . ') ' . substr($phone, 3, 3) . '-' . substr($phone, 6, 4);

		if(strlen('' . $phone) > 10)
		{
			$string .= ' x' . substr($phone, 10);
		}

		return $string;
	}

	return $default;
}


function displayTel($string, $default = '')
{
	if($phone = preg_replace('/[^0-9]/', '', $string))
	{
		return substr($phone, 0, 10);
	}

	return $default;
}

function displayImageLinkWidgetArea($acf_id)
{
	include('image-link-widget-area.php');
}