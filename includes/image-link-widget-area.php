<?php if(have_rows($acf_id)) : ?>
	<?php while(have_rows($acf_id)) : the_row(); ?>
		<?php
			$widget = [
				'background' => get_sub_field('background'),
				'color'      => get_sub_field('color'),
				'text'       => get_sub_field('text'),
				'image'      => get_sub_field('image')['url'],
				'link'       => get_sub_field('link'),
				'active'     => get_sub_field('active'),
			];
		?>
		<?php if($widget['active']) : ?>
			<?php if($widget['link']) : ?>
				<a class="image-link-widget"
					 href="<?php echo $widget['link']['url']; ?>"
					 target="<?php echo $widget['link']['target']; ?>"
				>
					<img src="<?php echo $widget['image']; ?>" />
					<div class="overlay"></div>
					<div class="ribbon" style="background-color: <?php echo $widget['background']; ?>;"></div>
					<div class="label" style="background-color: <?php echo $widget['background']; ?>;">
						<div class="triangle" style="border-color: <?php echo $widget['background']; ?>;"></div>
						<div class="text" style="color: <?php echo $widget['color']; ?>;"><?php echo $widget['text']; ?> &raquo;</div>
					</div>
				</a>
			<?php else : ?>
				<div class="image-link-widget">
					<img src="<?php echo $widget['image']; ?>" />
				</div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endwhile; ?>
<?php endif; ?>
