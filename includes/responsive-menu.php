<div id="responsive_menu">
	<div id="dl-menu" class="responsive-menu dl-menuwrapper">
		<button class="dl-trigger"><i class="fa fa-bars fa-lg" aria-hidden="true"></i> &nbsp;Menu</button>
		<ul class="dl-menu">
		<?php foreach($menu as $id => $menu_item) : ?>
			<?php if(!isset($menu_item['children'])) : ?>
				<li data-id="<?php echo $id; ?>" class="no-children">
					<a href="<?php echo $menu_item['url']; ?>"><?php echo $menu_item['name']; ?></a>
				</li>
				<?php else : ?>
					<li data-id="<?php echo $id; ?>" class="has-children">
						<a href="#"><?php echo $menu_item['name']; ?></a>
						<ul class="dl-submenu">
							<li><a href="<?php echo $menu_item['url']; ?>"><?php echo $menu_item['name']; ?></a></li>
							<?php foreach($menu_item['children'] as $child_id => $child) : ?>
								<li data-id="<?php echo $child_id; ?>">
									<a href="<?php echo $child['url']; ?>"><?php echo $child['name']; ?></a>
								</li>
							<?php endforeach; ?>
						</ul>
					</li>
				<?php endif; ?>
		<?php endforeach; ?>
		</ul>
	</div>
</div>
<div class="clearer"></div>