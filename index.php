<?php get_header(); ?>

<section id="main" class="home">
	<?php include(get_template_directory() . '/_templates/_partials/home-banner.php'); ?>
	<div class="content">
		<div class="home-col left">
			<div class="home-widget tweets">
                <h3>Tweets <a href="https://twitter.com/geparks" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/btn-twitter-follow.png" /></a></h3>
				<div class="twitter-embed-wrap">
					<a class="twitter-timeline" data-width="311" data-height="450" data-link-color="#2293c5" data-chrome="noheader nofooter transparent" href="https://twitter.com/geparks">Tweets by geparks</a>
					<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
				</div>
				<a href="https://twitter.com/geparks" class="widget-bottom-btn" target="_blank"><span style="font-weight: 400;">Tweet</span> @geparks &raquo;</a>
			</div>
		</div>
		<div class="home-col mid">
			<div class="home-widget news">
				<h3>Latest News</h3>
				<div class="content-wrap">
					<!----- Display Posts ----->
					<?php query_posts('order=DESC&posts_per_page=3'); ?>
					<?php while(have_posts()) : the_post(); ?>
						<div class="post">
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<?php $featured_image_link_url = get_post_meta(get_the_ID(), 'featured_image_link_url', true); ?>
							<?php if($featured_image_link_url && $featured_image_link_url != '') : ?>
								<a href="<?php echo $featured_image_link_url; ?>">
									<?php the_post_thumbnail('thumbnail', ['class' => 'alignleft']); ?>
								</a>
							<?php else : ?>
								<?php the_post_thumbnail('thumbnail', ['class' => 'alignleft']); ?>
							<?php endif; ?>
							<?php $content = the_content('Read More &raquo;'); ?>
							<p><?php echo $content; ?></p>
							<div class="clearer"></div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</div>
				<a href="<?php echo home_url('/'); ?>events-news/" class="widget-bottom-btn">All News &raquo;</a>
			</div>
		</div>
		<div class="home-col right">
			<div class="home-widget">
                <ul class="hot-buttons">
                    <li>
                        <a href="http://ackermansfc.com/center-features/group-exercise/">
                            <div class="icon">
                                <img src="http://ackermansfc.com/wp-content/uploads/2018/01/home_icon-events.png">
                            </div>
                            <div class="text">
                                <span class="title">Group Fitness Schedule</span>
                                <span class="description">
												Check out our upcoming classes																							</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="http://ackermansfc.com/center-features/personal-training/">
                            <div class="icon">
                                <img src="http://ackermansfc.com/wp-content/uploads/2018/01/weights-icon.png">
                            </div>
                            <div class="text">
                                <span class="title">Personal Training</span>
                                <span class="description">
												Super charge your workouts today																							</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="http://ackermansfc.com/center-features/open-gym-turf/">
                            <div class="icon">
                                <img src="http://ackermansfc.com/wp-content/uploads/2018/01/hot-button-open-gym.png">
                            </div>
                            <div class="text">
                                <span class="title">Open Gym &amp; Turf</span>
                                <span class="description">
												View open gym &amp; turf hours																							</span>
                            </div>
                        </a>
                    </li>
                </ul>
			</div>
			<div class="home-widget info mailchimp">
				<h5>Email Signup <cite>Stay connected with news &amp; updates</cite></h5>
				<!-- START MailChimp Form Embed -->
				<div id="mc_embed_signup">
					<form action="//gepark.us9.list-manage.com/subscribe/post?u=0519de60aadf81b3a43dd0a8d&amp;id=b69b9a5731" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
						<div id="mc_embed_signup_scroll">
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Email Address" />
							<div id="mce-responses" class="clear">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>
							<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
							<div style="position: absolute; left: -5000px;" aria-hidden="true">
								<input type="text" name="b_0519de60aadf81b3a43dd0a8d_b69b9a5731" tabindex="-1" value="" />
							</div>
							<div class="clear">
								<span class="btn-wrap">
									<input type="submit" value="Submit &raquo;" name="subscribe" id="mc-embedded-subscribe" class="button" />
								</span>
							</div>
						</div>
					</form>
				</div>
				<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
				<script type='text/javascript'>(function($)
						{
								window.fnames = new Array();
								window.ftypes = new Array();
								fnames[0] = 'EMAIL';
								ftypes[0] = 'email';
								fnames[1] = 'FNAME';
								ftypes[1] = 'text';
								fnames[2] = 'LNAME';
								ftypes[2] = 'text';
						}(jQuery));
						var $mcj = jQuery.noConflict(true);</script>
				<!-- END MailChimp Form Embed -->
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>