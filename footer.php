	<div class="foot-wrap">
		<footer id="footer">
			<div class="address col">
				<p>
					<strong>Ackerman SFC</strong><br />
					800 St. Charles Rd<br />
					Glen Ellyn, IL 60137<br />
					<a href="https://www.google.com/maps/place/Ackerman+Sports+%26+Fitness+Center/@41.8930764,-88.0650758,15z/data=!4m5!3m4!1s0x880e52c460d06ba7:0xb04885dfddcc8e37!8m2!3d41.8930764!4d-88.0563211" target="_blank">[+] Google Maps</a>
				</p>
				<a href="tel:+16303170130" class="icon-link">(630) 317-0130</a>
				<!--<a href="mailto:info@ackermansfc.com" class="icon-link">info@ackermansfc.com</a>-->
			</div>
			<div class="col mission">
				<h4>Mission Statement</h4>
				<p>Our mission is driven to foster diverse, community-based leisure opportunities, through a harmonious blend of quality recreation programs, facilities and open space which will enhance the quality of life into the future.</p>
			</div>
			<div class="links-1 col footer-nav">
				<ul>
					<li><a href="<?php echo home_url('/'); ?>">Home</a></li>
					<li><a href="<?php echo home_url('/'); ?>about/hours/">Hours & Location</a></li>
					<li><a href="<?php echo home_url('/'); ?>join/membership-options/">Membership</a></li>
					<li><a href="<?php echo home_url('/'); ?>join/member-benefits/">Benefits</a></li>
					<li><a href="<?php echo home_url('/'); ?>rentals/gym-turf/">Rentals</a></li>
				</ul>
			</div>
			<div class="links-2 col footer-nav">
				<ul>
					<li><a href="<?php echo home_url('/'); ?>events/">Upcoming Events</a></li>
					<li><a href="<?php echo home_url('/'); ?>center-features/open-gym-turf/">Open Gym & Turf</a></li>
					<li><a href="<?php echo home_url('/'); ?>center-features/group-exercise/">Group Fitness</a></li>
					<li><a href="<?php echo home_url('/'); ?>for-kids/birthday-parties/">Birthday Parties</a></li>
					<li><a href="<?php echo home_url('/'); ?>contact/">Contact</a></li>
				</ul>
			</div>
			<div class="copyright col">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/logo-footer.png" />
				<span>Ackerman SFC &copy; <?php echo date('Y'); ?><br />Powered by <a href="https://www.invexdesign.com" target="_blank" title="Chicago Web Design, Web Hosting, SEO | Invex Design"><strong>Invex Design</strong></a></span>
			</div>
			<span class="overline"> </span>
		</footer>
	</div>
	<script>
		$(document).ready(function()
		{
			/* Stachethemes Calendar - Grid - Links => open in new window */
			$('.stec .stec-layout-grid a').prop('target', '_blank')
		});
	</script>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-39685632-1', 'auto');
		ga('send', 'pageview');

	</script>

	<?php wp_footer(); ?>
</body>
</html>