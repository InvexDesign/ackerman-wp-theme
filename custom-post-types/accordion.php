<?php

function register_accordion_custom_post_type()
{
	// Set UI labels for Custom Post Type
	$labels = [
		'name'               => _x('Accordions', 'Post Type General Name', 'twentythirteen'),
		'singular_name'      => _x('Accordion', 'Post Type Singular Name', 'twentythirteen'),
		'menu_name'          => __('Accordions', 'twentythirteen'),
		'parent_item_colon'  => __('Parent Accordion', 'twentythirteen'),
		'all_items'          => __('All Accordions', 'twentythirteen'),
		'view_item'          => __('View Accordion', 'twentythirteen'),
		'add_new_item'       => __('Add New Accordion', 'twentythirteen'),
		'add_new'            => __('Add New', 'twentythirteen'),
		'edit_item'          => __('Edit Accordion', 'twentythirteen'),
		'update_item'        => __('Update Accordion', 'twentythirteen'),
		'search_items'       => __('Search Accordion', 'twentythirteen'),
		'not_found'          => __('Not Found', 'twentythirteen'),
		'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
	];

	// Set other options for Custom Post Type
	$args = [
		'label'               => __('accordions', 'twentythirteen'),
		'description'         => __('Accordion', 'twentythirteen'),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => [
			'title',
//			'page-attributes',
//			'editor',
//			'excerpt',
//			'author',
//			'thumbnail',
//			'comments',
//			'revisions',
			'custom-fields',
		],
		// You can associate this CPT with a taxonomy or custom taxonomy.
		'taxonomies'          => ['genres'],
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/
		'hierarchical'        => true,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-media-default',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
	];

	// Registering your Custom Post Type
	register_post_type('accordion', $args);
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action('init', 'register_accordion_custom_post_type', 0);

function accordion_add_meta_boxes($post)
{
	add_meta_box('accordion_shortcode_meta_box', 'Displaying Your Accordion', 'accordion_build_shortcode_meta_box', 'accordion', 'side', 'low');
}
add_action('add_meta_boxes_accordion', 'accordion_add_meta_boxes');

function accordion_build_shortcode_meta_box($post)
{
	?>
	<p>To display your accordion add the following shortcode to your page content. Clicking the shortcode below will copy it to your clipboard. This shortcode also accepts an optional "style" attribute to allow custom CSS styles.</p>
	<span class="shortcode-copy">[accordion slug="<?php echo $post->post_name; ?>"]</span>
	<?php
}