<?php

function getPostModifiedAtTimezonedString($post_id)
{
	$modified_at_utc_string = get_post_field('post_modified_gmt', $post_id);
	$modified_at = new DateTime($modified_at_utc_string);
	$modified_at->setTimezone(new DateTimeZone('America/Chicago'));

	return $modified_at->format('c');
}

function _getAllPopups()
{
	$success = true;

	$emergencyPopup = get_field('emergency_popup', 'option');
	if(isset($emergencyPopup['popup']) && $emergencyPopup['popup']) {
		$emergencyPopup['modifiedAt'] = getPostModifiedAtTimezonedString($emergencyPopup['popup']);
	}

	$pageSpecificPopups = get_field('page_specific_popups', 'option');
	foreach($pageSpecificPopups as $index => $popup)
	{
		if(isset($popup['popup']) && $popup['popup']) {
			$pageSpecificPopups[$index]['modifiedAt'] = getPostModifiedAtTimezonedString($popup['popup']);
		}
	}

	$globalPopups = get_field('global_popups', 'option');
	foreach($globalPopups as $index => $popup)
	{
		if(isset($popup['popup']) && $popup['popup']) {
			$globalPopups[$index]['modifiedAt'] = getPostModifiedAtTimezonedString($popup['popup']);
		}
	}

	return compact('success', 'emergencyPopup', 'pageSpecificPopups', 'globalPopups');
}

function _getPopupById(WP_REST_Request $request)
{
	$popup_id = $request->get_param('popupId');

	$post = get_post($popup_id);
	$fields = get_fields($popup_id);

	ob_start();
	include(get_template_directory() . '/invex-popups/partials/popup.php');
	$markup = ob_get_clean();

	return compact('post', 'fields', 'markup');
}

function _getPopupModifiedAtById(WP_REST_Request $request)
{
	$popup_id = $request->get_param('popupId');

	$modified_at_utc_string = get_post_field('post_modified_gmt', $popup_id);
	$modified_at = new DateTime($modified_at_utc_string);
	$modified_at->setTimezone(new DateTimeZone('America/Chicago'));

	return $modified_at->format('c');
}

add_action('rest_api_init', function()
{
	register_rest_route('popups/v1', '/all', [
		'methods'  => 'get',
		'callback' => '_getAllPopups',
	]);
	register_rest_route('popups/v1', '/by-id', [
		'methods'  => 'POST',
		'callback' => '_getPopupById',
	]);
});
