Instructions:
- copy "invex-popups" directory to theme
- add the following line to functions.php:
    - require_once('invex-popups/invex-popups.php');
- examine popup styles and add to "popup-theme-adjustments.css" to address any css conflicts
- look for php function declaration conflicts (ie Invex Access Manager)
- to debug, temporarily set "window.DEBUG_POPUP" to true in invex-popups.php