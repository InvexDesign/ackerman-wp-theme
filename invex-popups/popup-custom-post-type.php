<?php

define('INVEX_POPUP_ID_PREFIX', 'invex_popup_');

function register_popup_custom_post_type()
{
	// Set UI labels for Custom Post Type
	$labels = [
		'name'               => _x('Popups', 'Post Type General Name', 'twentythirteen'),
		'singular_name'      => _x('Popup', 'Post Type Singular Name', 'twentythirteen'),
		'menu_name'          => __('Popups', 'twentythirteen'),
		'parent_item_colon'  => __('Parent Popup', 'twentythirteen'),
		'all_items'          => __('All Popups', 'twentythirteen'),
		'view_item'          => __('View Popup', 'twentythirteen'),
		'add_new_item'       => __('Add New Popup', 'twentythirteen'),
		'add_new'            => __('Add New', 'twentythirteen'),
		'edit_item'          => __('Edit Popup', 'twentythirteen'),
		'update_item'        => __('Update Popup', 'twentythirteen'),
		'search_items'       => __('Search Popup', 'twentythirteen'),
		'not_found'          => __('Not Found', 'twentythirteen'),
		'not_found_in_trash' => __('Not found in Trash', 'twentythirteen'),
	];

	// Set other options for Custom Post Type
	$args = [
		'label'               => __('popups', 'twentythirteen'),
		'description'         => __('Popup', 'twentythirteen'),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => [
			'title',
			'custom-fields',
		],
		// You can associate this CPT with a taxonomy or custom taxonomy.
		'taxonomies'          => [],
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/
		'hierarchical'        => true,
		'public'              => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-align-center',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
	];

	// Registering your Custom Post Type
	register_post_type('popup', $args);
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/
add_action('init', 'register_popup_custom_post_type', 0);

function add_popup_preview_button_to_wp_admin_bar($admin_bar)
{
	global $post;
	global $pagenow;

	if($pagenow == 'post.php' && $post->post_type == 'popup')
	{
		$admin_bar->add_menu([
			'title' => 'Preview Popup',
			'href'  => home_url() . '?preview-popup=' . $post->ID,
			'meta'  => ['target' => '_blank']
		]);
	}
}
add_action('admin_bar_menu', 'add_popup_preview_button_to_wp_admin_bar', 100);

function renderAcfConfigurablePopupStyles($color_scheme, $type = null)
{
	include(get_template_directory() . '/invex-popups/partials/acf-configurable-styles.php');
}

function render_acf_configurable_popup_styles()
{
	$emergency_popup_color_scheme = get_field('emergency_popup_color_scheme', 'option');
	$general_popup_color_scheme = get_field('general_popup_color_scheme', 'option');

	renderAcfConfigurablePopupStyles($general_popup_color_scheme);
	renderAcfConfigurablePopupStyles($emergency_popup_color_scheme, 'emergency');
}

add_action('wp_head', 'render_acf_configurable_popup_styles');
