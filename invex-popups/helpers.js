function findGetParameter(parameterName)
{
	var result = null,
			tmp = [];

	location.search
	.substr(1)
	.split("&")
	.forEach(function(item) {
		tmp = item.split("=");
		if(tmp[0] === parameterName)
		{
			result = decodeURIComponent(tmp[1]);
		}
	});

	return result;
}

function isScrolledIntoView(elem, offset)
{
	if(!offset) {
		offset = 0;
	}

	var docViewTop = $(window).scrollTop();
	var docViewBottom = docViewTop + window.innerHeight;

	var elemTop = $(elem).offset().top;

	return docViewBottom >= (elemTop + offset);
}

function smartScroll(target, options)
{
	var $target = $(target);
	if($target.length < 1) {
		return;
	}

	console.log($target);

	options = options ? options : {};

	var offset = options.offset || 0;
	var speed = options.speed || 750;

	$('html, body').animate({
		scrollTop: $target.offset().top + offset
	}, speed);

	return $target;
}

function prepareGetVariables()
{
	var $_GET = {};
	if(document.location.toString().indexOf('?') !== -1)
	{
		var query = document.location
		.toString()// get the query string
		.replace(/^.*?\?/, '')// and remove any existing hash string (thanks, @vrijdenker)
		.replace(/#.*$/, '')
		.split('&');

		for(var i = 0, l = query.length; i < l; i++)
		{
			var aux = decodeURIComponent(query[i]).split('=');
			$_GET[aux[0]] = aux[1];
		}
	}

	window['$_GET'] = $_GET;
}