<?php
/*
Template Name: Full Width
*/
?>
<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<section id="main" class="main full-width">
			<?php
			$banner_title = getAdvancedCustomFieldValue('banner-title', false);
			$banner_image = getAdvancedCustomFieldValue('banner-image', false);
			if(!$banner_image && $post->post_parent)
			{
				$banner_image = getAdvancedCustomFieldValue('banner-image', false, $post->post_parent);
			}

			if(!$banner_title && $post->post_parent)
			{
				$banner_title = getAdvancedCustomFieldValue('banner-title', false, $post->post_parent);
			}

			if($banner_title && $banner_image)
			{
				$banner_image_url = $banner_image['url'];
				include(get_template_directory() . '/_templates/_partials/short-banner.php');
			}
			?>
			<div class="content">
				<div class="main-col">
					<?php $page_title = getAdvancedCustomFieldValue('custom-page-title', get_the_title()); ?>
					<h3><?php echo $page_title; ?><?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
					<div class="content-wrap">
					  <?php the_content(); ?>
						<div style="clear: both;"></div>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>