<?php
/*
Template Name: Search
*/
?>

<?php get_header(); ?>

<section id="main" class="main full-width">
		<div class="content">
			<div class="main-col full-width">
				<h3>Search</h3>
				<div class="content-wrap">
					<form method="get" id="searchform" action="<?php echo home_url(); ?>">
						<input type="text" style="margin-left: -6px;width: 100%;" value="Search" id="s" name="s" onfocus="if(this.value=='Search')this.value=''" onblur="if(this.value=='')this.value='Search'">
						<input type="image" src="<?php echo get_template_directory_uri(); ?>/assets/build/images/search_btn.png" id="search-btn">
					</form>
				</div>
			</div>
		</div>
</section>

<?php get_footer(); ?>
