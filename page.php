<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>
		<section id="main">
			<?php
	  		$banner_title = getAdvancedCustomFieldValue('banner-title', false);
			  $banner_image = getAdvancedCustomFieldValue('banner-image', false);
	  		if(!$banner_image && $post->post_parent)
				{
					$banner_image = getAdvancedCustomFieldValue('banner-image', false, $post->post_parent);
				}

				if(!$banner_title && $post->post_parent)
				{
					$banner_title = getAdvancedCustomFieldValue('banner-title', false, $post->post_parent);
				}

	  		$banner_image_url = $banner_image ? $banner_image['url'] : get_template_directory_uri() . '/assets/build/images/default-banner.png';
				include(get_template_directory() . '/_templates/_partials/short-banner.php');
			?>
			<div class="content">
				<?php
					$sidebar_menu_id = getAdvancedCustomFieldValue('sidebar-menu', false);
					if(!$sidebar_menu_id && $post->post_parent)
					{
						$sidebar_menu_id = getAdvancedCustomFieldValue('sidebar-menu', false, $post->post_parent);
					}

					$widgets = [];
					if($post->post_parent)
					{
						if(have_rows('sidebar-widget', $post->post_parent))
						{
							while(have_rows('sidebar-widget', $post->post_parent))
							{
								the_row();

								$widget = [
									'title' => get_sub_field('title'),
									'color' => get_sub_field('color'),
									'content' => get_sub_field('content')
							  ];
								$widgets[] = $widget;
							}
						}
					}

					if(have_rows('sidebar-widget'))
					{
						while(have_rows('sidebar-widget'))
						{
							the_row();

							$widget = [
								'title' => get_sub_field('title'),
								'color' => get_sub_field('color'),
								'content' => get_sub_field('content')
							];
							$widgets[] = $widget;
						}
					}
					include(get_template_directory() . '/_templates/_partials/sidebar.php');
				?>
				<div class="main-col">
					<?php $page_title = getAdvancedCustomFieldValue('custom-page-title', get_the_title()); ?>
					<h3><?php echo $page_title; ?> <?php include(get_template_directory() . '/_templates/_partials/sharethis.php'); ?></h3>
					<div class="content-wrap">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>