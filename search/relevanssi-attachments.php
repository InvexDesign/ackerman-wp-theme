<?php

function rlv_only_pdfs($restriction)
{
	global $wpdb;

	$restriction .= " AND post.ID NOT IN (SELECT ID FROM $wpdb->posts WHERE post_type = 'attachment' AND post_mime_type != 'application/pdf' AND post_mime_type != 'application/msword' ) ";

	return $restriction;
}
add_filter('relevanssi_indexing_restriction', 'rlv_only_pdfs');
