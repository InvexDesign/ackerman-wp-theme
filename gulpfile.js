var gulp = require('gulp'),
    concat = require('gulp-concat'),
    deporder = require('gulp-deporder'),
    stripdebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    assets = require('postcss-assets'),
    autoprefixer = require('autoprefixer'),
    mqpacker = require('css-mqpacker'),
    cssnano = require('cssnano'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    devBuild = (process.env.NODE_ENV !== 'production'),// development mode?
    folder = {
        source: 'assets/source/',
        build: 'assets/build/',
        node: 'node_modules/'
    }
;

gulp.task('js', function()
{
    var jsbuild = gulp.src([
            folder.node + 'jquery/dist/jquery.min.js',
            folder.node + 'bootstrap/dist/js/bootstrap.min.js',
            folder.node + 'headhesive/dist/headhesive.min.js',
            folder.node + 'jquery.cycle2/src/jquery.cycle2.min.js',
            folder.node + 'lightbox2/dist/js/lightbox.min.js',
            folder.node + 'lity/dist/lity.min.js',
            folder.node + 'responsivemultilevelmenu/js/modernizr.custom.js',
            folder.node + 'responsivemultilevelmenu/js/jquery.dlmenu.js',
            folder.source + 'js/**/*'
        ])
        .pipe(deporder())
        .pipe(concat('bundle.min.js'));

    if(!devBuild)
    {
        jsbuild = jsbuild
            .pipe(stripdebug())
            .pipe(uglify());
    }

    return jsbuild.pipe(gulp.dest(folder.build + 'js/'));
});

gulp.task('js-alt', function()
{
    var jsbuild = gulp.src([
            folder.node + 'jquery/dist/jquery.min.js',
            folder.node + 'bootstrap/dist/js/bootstrap.min.js',
            folder.node + 'headhesive/dist/headhesive.min.js',
            folder.node + 'jquery.cycle2/src/jquery.cycle2.min.js',
            folder.node + 'lightbox2/dist/js/lightbox.min.js',
            folder.node + 'lity/dist/lity.min.js',
            folder.node + 'responsivemultilevelmenu/js/modernizr.custom.js',
            folder.node + 'responsivemultilevelmenu/js/jquery.dlmenu.js',
            folder.source + 'js/init-headhesive.js',
            folder.source + 'init-responsive-menu-alt.js'
        ])
        .pipe(deporder())
        .pipe(concat('bundle.min.js'));

    if(!devBuild)
    {
        jsbuild = jsbuild
            .pipe(stripdebug())
            .pipe(uglify());
    }

    return jsbuild.pipe(gulp.dest(folder.build + 'js/'));
});

gulp.task('images', function()
{
    var out = folder.build + 'images/';
    return gulp.src([
            folder.node + 'lightbox2/dist/images/*',
            folder.source + 'images/**/*'
        ])
        .pipe(newer(out))
        .pipe(imagemin({optimizationLevel: 5}))
        .pipe(gulp.dest(out));
});

gulp.task('etc', function()
{
    var out = folder.build + 'etc/';
    return gulp.src([
            folder.source + 'etc/**/*'
        ])
        .pipe(gulp.dest(out));
});

gulp.task('favicon', function()
{
    var out = folder.build + 'favicon/';
    return gulp.src([
            folder.source + 'favicon/**/*'
        ])
        .pipe(gulp.dest(out));
});

gulp.task('fonts', function()
{
    var out = folder.build + 'fonts/';
    return gulp.src([
            folder.node + 'font-awesome/fonts/*',
            folder.node + 'responsivemultilevelmenu/fonts/*',
            folder.source + 'fonts/*'
        ])
        .pipe(gulp.dest(out));
});

gulp.task('css', ['images'], function()
{
    var postCssOpts = [
        assets({loadPaths: ['images/']}),
        autoprefixer({browsers: ['last 2 versions', '> 2%']}),
        mqpacker
    ];

    if(!devBuild)
    {
        postCssOpts.push(cssnano);
    }

    return gulp.src([
            folder.node + 'font-awesome/css/font-awesome.min.css',
            folder.node + 'responsivemultilevelmenu/css/component.css',
            folder.node + 'lightbox2/dist/css/lightbox.min.css',
            folder.node + 'lity/dist/lity.min.css',
            folder.source + 'css/variables.scss',
            folder.source + 'css/**/*'
        ])
        .pipe(sass({
            outputStyle: 'nested',
            imagePath: 'images/',
            precision: 3,
            errLogToConsole: true
        }))
        .pipe(concat('app.min.css'))
        .pipe(postcss(postCssOpts))
        .pipe(gulp.dest(folder.build + 'css/'));
});

gulp.task('watch', function()
{
    gulp.watch(folder.source + 'css/**/*', ['css']);
    gulp.watch(folder.source + 'etc/**/*', ['etc']);
    gulp.watch(folder.source + 'images/**/*', ['images']);
    gulp.watch(folder.source + 'js/**/*', ['js']);
    gulp.watch(folder.source + 'favicon/**/*', ['favicon']);
});

gulp.task('run', ['css', 'etc', 'fonts', 'js', 'favicon']);
gulp.task('run-alt', ['css', 'etc', 'fonts', 'js-alt', 'favicon']);
// gulp.task('default', ['run', 'watch']);