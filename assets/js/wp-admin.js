copySelectedText = function(element)
{
    var range, selection;

    if(window.getSelection)
    {
        selection = window.getSelection();
        range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
    else if(document.body.createTextRange)
    {
        range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    }

    try
    {
        if(document.execCommand('copy'))
        {
            alert('Copied to clipboard!');
        }
    }
    catch(err)
    {
        console.log('Couldn\'t copy the text');
    }
};

jQuery(document).ready(function($)
{
    $('.shortcode-copy').on('click', function()
    {
        copySelectedText(this);
    });
});