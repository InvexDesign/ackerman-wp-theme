var headhesive = false;
function initializeHeadhesive()
{
    if($(window).width() <= 1150)
    {
        if(headhesive != false)
        {
            headhesive.destroy();
            headhesive = false;
        }
    }
    else
    {
        if(headhesive == false)
        {
            headhesive = new Headhesive('div.head-wrap', {offset: 160});
        }
    }
}
$(document).ready(function()
{
    initializeHeadhesive();
    $(window).resize(function()
    {
        initializeHeadhesive();
    });
});