<?php $CURRENT_URL = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Ackerman Sports & Fitness Center | Glen Ellyn, Illinois</title>

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800,400italic,300italic'
				rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>

	<?php include('includes/favicon.php'); ?>
	<?php include('includes/share-this.php'); ?>
	<?php include('includes/google-analytics.php'); ?>

	<?php wp_head(); ?>
</head>
<body
		<?php body_class((get_browser_name($_SERVER['HTTP_USER_AGENT']) == 'Internet Explorer') ? 'internet-explorer' : ''); ?>
		data-user-agent="<?php echo $_SERVER['HTTP_USER_AGENT']; ?>"
		data-browser="<?php echo get_browser_name($_SERVER['HTTP_USER_AGENT']); ?>">
<?php include('includes/emergency-update.php'); ?>
<div class="head-wrap">
	<header id="header">
		<div class="meta">
			<ul class="social">
				<li>
					<a href="<?php echo FACEBOOK_URL; ?>" title="Like GEPD on Facebook" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/social-facebook.png" alt="Facebook Icon" />
					</a>
				</li>
				<li>
					<a href="<?php echo TWITTER_URL; ?>" title="Follow GEPD on Twitter" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/social-twitter.png" alt="Twitter Icon" />
					</a>
				</li>
				<li>
					<a href="<?php echo INSTAGRAM_URL; ?>" title="Find GEPD on Instagram" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/social-instagram-blue.png" alt="Instagram Icon" />
					</a>
				</li>
			</ul>
			<ul class="meta-nav">
				<li><a href="<?php echo home_url('/'); ?>center-features/open-gym-turf/#schedule">Gym &amp; Turf</a></li>
				<li><a href="<?php echo home_url('/'); ?>group-exercise/">Group Exercise</a></li>
				<li><a href="<?php echo home_url('/'); ?>contact/">Contact</a></li>
				<li class="magnifying-glass">
					<a href="<?php echo home_url('/'); ?>?s=" style="display: flex; justify-content: center; align-items: center;">
						<i style="font-size: 1.5em;" class="fa fa-search" aria-hidden="true"></i>&nbsp;
					</a>
				</li>
			</ul>
		<?php get_search_form(); ?>
		</div>
		<h1 class="logo">
			<a href="<?php echo home_url('/'); ?>" title="Glen Ellyn Park District">Glen Ellyn Park District</a>
		</h1>
		<h1 class="headhesive-logo logo">
			<a href="<?php echo home_url('/'); ?>" title="Glen Ellyn Park District">Glen Ellyn Park District</a>
		</h1>
		<div class="callouts">
			<div class="activity-guide hover-sweep-to-right one">
				<span class="text">Activity Guide</span>
				<a href="https://gepark.org/activity-guide" class="activity-guide trigger"></a>
				<div class="sweeper"></div>
				<div class="background"></div>
			</div>
			<div class="register-now hover-sweep-to-right two">
				<span class="text">Register Now</span>
				<a href="https://apm.activecommunities.com/gepark/Home" class="register-now trigger"></a>
				<div class="sweeper"></div>
				<div class="background"></div>
			</div>
		</div>
		<ul id="nav" class="nav">
			<span class="the-thin-blue-line left"></span>
			<?php $header_menu_items = preProcessMenuItems(HEADER_NAV_MENU_ID); ?>
			<?php foreach($header_menu_items as $id => $menu_item) : ?>
				<?php if(!isset($menu_item['children'])) : ?>
					<li data-id="<?php echo $id; ?>" class="no-children">
						<a href="<?php echo $menu_item['url']; ?>">
				  		<?php echo $menu_item['name']; ?>
						</a>
					</li>
				<?php else : ?>
					<li data-id="<?php echo $id; ?>" class="has-children">
						<a href="<?php echo $menu_item['url']; ?>">
						  <?php echo $menu_item['name']; ?>
							<div class="tab-padding"></div>
						</a>
						<div class="dropdown-menu" style="display: none;">
							<ul class="has-separators">
								<?php foreach($menu_item['children'] as $child_id => $child) : ?>
									<li data-id="<?php echo $child_id; ?>">
										<a href="<?php echo $child['url']; ?>"><?php echo $child['name']; ?></a>
									</li>
								<?php endforeach; ?>
							</ul>
							<span class="corner"></span>
						</div>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
			<span class="the-thin-blue-line right"></span></ul>
	</header>
	<?php displayMultilevelResponsiveMenu(HEADER_NAV_MENU_ID); ?>
</div>
